'use strict'

module.exports = function clean (str) {
  return str.replace(/\s+/g, ' ').trim()
}
