'use strict'

const childProcess = require('child_process')

module.exports = function exec (file, args, opts = Object.create(null)) {
  return new Promise((resolve, reject) => {
    childProcess.execFile(file, args, opts, (err, stdout, stderr) => {
      if (err) reject(err)
      else resolve({ stdout, stderr })
    })
  })
}
