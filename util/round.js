'use strict'

module.exports = function round (n, i = 0) {
  const f = 10 ** i
  return Math.round(n * f) / f
}
