'use strict'

const exec = require('../util/exec')
const clean = require('../util/clean')

exports.switchresToModeline = async function switchresToModeline (width, height, refresh, monitor, interlace = false) {
  const args = ['--calc', width, height, refresh, '--monitor', monitor]
  if (!interlace) args.push('--nointerlace')
  const res = await exec('switchres', args)
  const cleanModeline = clean(res.stdout.split('\n')[1]).toLowerCase().split(' ').slice(1).join(' ')
  const modeArgs = cleanModeline.match(/"(\d+)x(\d+)(?:.*)"\s+([\d.]+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/i).slice(3).map((n) => Number.parseFloat(n))
  return modeArgs
}

exports.tvOff = async function tvOff () {
  await exec('tvservice', ['-o'])
}

exports.tvOn = async function tvOn () {
  await exec('tvservice', ['-e', 'DMT 87'])
}

exports.setHdmiTimings = async function setHdmiTimings (hdmiTimings) {
  await exec('vcgencmd', ['hdmi_timings', ...hdmiTimings])
}

exports.setFbSize = async function setFbSize (width, height) {
  await exec('fbset', ['-depth', 16])
  await exec('fbset', ['-g', width, height, width, height, 24])
}

// hdmi_cvt=<width> <height> <framerate> <aspect> <margins> <interlace> <rb>
exports.setHdmiCvt = async function setHdmiCvt (width, height, refresh) {
  const res = await exec('vcgencmd', ['hdmi_cvt', width, height, refresh, 1, 0, 0, 0])
  return res.stdout
}

exports.getHdmiTimings = async function getHdmiTimings () {
  const res = await exec('vcgencmd', ['hdmi_timings'])
  return res.stdout
}
