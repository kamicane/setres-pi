'use strict'

const round = require('../util/round')

class Timings {
  constructor ([
    pClockHz,
    hActive, hFrontPorch, hSyncWidth, hBackPorch,
    vActive, vFrontPorch, vSyncWidth, vBackPorch
  ]) {
    this.pClockHz = pClockHz
    this.hActive = hActive
    this.hFrontPorch = hFrontPorch
    this.hSyncWidth = hSyncWidth
    this.hBackPorch = hBackPorch
    this.vActive = vActive
    this.vFrontPorch = vFrontPorch
    this.vSyncWidth = vSyncWidth
    this.vBackPorch = vBackPorch
  }

  get hTotal () {
    return this.hActive + this.hFrontPorch + this.hSyncWidth + this.hBackPorch
  }

  get vTotal () {
    return this.vActive + this.vFrontPorch + this.vSyncWidth + this.vBackPorch
  }

  get pClockMhz () {
    return this.pClockHz / 1000000
  }

  get pClockKhz () {
    return this.pClockHz / 1000
  }

  get hSyncStart () {
    return this.hActive + this.hFrontPorch
  }

  get hSyncEnd () {
    return this.hActive + this.hFrontPorch + this.hSyncWidth
  }

  get vSyncStart () {
    return this.vActive + this.vFrontPorch
  }

  get vSyncEnd () {
    return this.vActive + this.vFrontPorch + this.vSyncWidth
  }

  get hFreqKhz () {
    return this.pClockKhz / this.hTotal
  }

  get vScanRateHz () {
    return 1000 * this.hFreqKhz / this.vTotal
  }

  hOffset (n) {
    if (n === 0) return this

    if (n > 0) {
      const hFrontPorch = this.hFrontPorch - n
      if (hFrontPorch < 1) {
        n = n + hFrontPorch - 1
      }
    } else {
      const hBackPorch = this.hBackPorch + n
      if (hBackPorch < 1) {
        n = n - hBackPorch + 1
      }
    }

    this.hBackPorch += n
    this.hFrontPorch -= n

    return this
  }

  vOffset (n) {
    if (n === 0) return this

    if (n > 0) {
      const vFrontPorch = this.vFrontPorch - n
      if (vFrontPorch < 1) {
        n = n + vFrontPorch - 1
      }
    } else {
      const vBackPorch = this.vBackPorch + n
      if (vBackPorch < 1) {
        n = n - vBackPorch + 1
      }
    }

    this.vBackPorch += n
    this.vFrontPorch -= n

    return this
  }

  static fromModeline ([
    pClockMhz,
    hActive, hSyncStart, hSyncEnd, hTotal,
    vActive, vSyncStart, vSyncEnd, vTotal
  ]) {
    const pClockHz = round(pClockMhz * 1000000)
    const hFrontPorch = hSyncStart - hActive
    const hSyncWidth = hSyncEnd - hSyncStart
    const hBackPorch = hTotal - hSyncEnd

    const vFrontPorch = vSyncStart - vActive
    const vSyncWidth = vSyncEnd - vSyncStart
    const vBackPorch = vTotal - vSyncEnd

    return new Timings([
      pClockHz,
      hActive, hFrontPorch, hSyncWidth, hBackPorch,
      vActive, vFrontPorch, vSyncWidth, vBackPorch
    ])
  }

  static fromHdmiTimings ([
    hActive, hSyncPolarity, hFrontPorch, hSyncWidth, hBackPorch,
    vActive, vSyncPolarity, vFrontPorch, vSyncWidth, vBackPorch,
    vSyncOffsetA, vSyncOffsetB, pixelRep,
    vScanRateHz, interlaced, pClockHz, aspectRatio
  ]) {
    return new Timings([
      pClockHz,
      hActive, hFrontPorch, hSyncWidth, hBackPorch,
      vActive, vFrontPorch, vSyncWidth, vBackPorch
    ])
  }

  toHdmiTimings () {
    return [
      this.hActive, 1, this.hFrontPorch, this.hSyncWidth, this.hBackPorch,
      this.vActive, 1, this.vFrontPorch, this.vSyncWidth, this.vBackPorch,
      0, 0, 0, round(this.vScanRateHz, 2), 0, round(this.pClockHz), 1
    ]
  }

  toModeline () {
    return [
      this.pClockMhz,
      this.hActive, this.hSyncStart, this.hSyncEnd, this.hTotal,
      this.vActive, this.vSyncStart, this.vSyncEnd, this.vTotal
    ]
  }
}

module.exports = Timings
